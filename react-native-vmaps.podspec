require 'json'

package = JSON.parse(File.read(File.join(__dir__, 'package.json')))

Pod::Spec.new do |s|
  s.name		= "react-native-vmaps"
  s.summary		= "Bản Đồ VMaps"
  s.version		= package['version']
  s.authors		= { "Caka" => "caka.ict@gmail.com" }
  s.homepage    	= "https://gitlab.com/caka.ict/react-native-vmaps/-/blob/master/README.md"
  s.license     	= "MIT"
  s.platform    	= :ios, "8.0"
  s.source      	= { :git => "https://gitlab.com/caka.ict/react-native-vmaps.git" }
  s.source_files	= "ios/RCTMGL/**/*.{h,m}"

  s.dependency 'Mapbox-iOS-SDK', '~> 5.7'
  s.dependency 'React'
end
