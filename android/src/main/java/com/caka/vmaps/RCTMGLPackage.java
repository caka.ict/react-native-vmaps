package com.caka.vmaps;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.JavaScriptModule;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.caka.vmaps.components.annotation.RCTMGLCalloutManager;
import com.caka.vmaps.components.annotation.RCTMGLPointAnnotationManager;
import com.caka.vmaps.components.annotation.RCTMGLMarkerViewManager;
import com.caka.vmaps.components.camera.RCTMGLCameraManager;
import com.caka.vmaps.components.images.RCTMGLImagesManager;
import com.caka.vmaps.components.mapview.RCTMGLMapViewManager;
import com.caka.vmaps.components.mapview.RCTMGLAndroidTextureMapViewManager;
import com.caka.vmaps.components.styles.layers.RCTMGLBackgroundLayerManager;
import com.caka.vmaps.components.styles.layers.RCTMGLCircleLayerManager;
import com.caka.vmaps.components.styles.layers.RCTMGLFillExtrusionLayerManager;
import com.caka.vmaps.components.styles.layers.RCTMGLFillLayerManager;
import com.caka.vmaps.components.styles.layers.RCTMGLHeatmapLayerManager;
import com.caka.vmaps.components.styles.layers.RCTMGLLineLayerManager;
import com.caka.vmaps.components.styles.layers.RCTMGLRasterLayerManager;
import com.caka.vmaps.components.styles.layers.RCTMGLSymbolLayerManager;
import com.caka.vmaps.components.styles.light.RCTMGLLightManager;
import com.caka.vmaps.components.styles.sources.RCTMGLImageSourceManager;
import com.caka.vmaps.components.styles.sources.RCTMGLRasterSourceManager;
import com.caka.vmaps.components.styles.sources.RCTMGLShapeSourceManager;
import com.caka.vmaps.components.styles.sources.RCTMGLVectorSourceManager;
import com.caka.vmaps.modules.RCTMGLLocationModule;
import com.caka.vmaps.modules.RCTMGLModule;
import com.caka.vmaps.modules.RCTMGLOfflineModule;
import com.caka.vmaps.modules.RCTMGLSnapshotModule;

/**
 * Created by nickitaliano on 8/18/17.
 */

public class RCTMGLPackage implements ReactPackage {

    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactApplicationContext) {
        List<NativeModule> modules = new ArrayList<>();

        modules.add(new RCTMGLModule(reactApplicationContext));
        modules.add(new RCTMGLOfflineModule(reactApplicationContext));
        modules.add(new RCTMGLSnapshotModule(reactApplicationContext));
        modules.add(new RCTMGLLocationModule(reactApplicationContext));

        return modules;
    }

    @Deprecated
    public List<Class<? extends JavaScriptModule>> createJSModules() {
        return Collections.emptyList();
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactApplicationContext) {
        List<ViewManager> managers = new ArrayList<>();

        // components
        managers.add(new RCTMGLCameraManager(reactApplicationContext));
        managers.add(new RCTMGLMapViewManager(reactApplicationContext));
        managers.add(new RCTMGLMarkerViewManager(reactApplicationContext));
        managers.add(new RCTMGLAndroidTextureMapViewManager(reactApplicationContext));
        managers.add(new RCTMGLLightManager());
        managers.add(new RCTMGLPointAnnotationManager(reactApplicationContext));
        managers.add(new RCTMGLCalloutManager());

        // sources
        managers.add(new RCTMGLVectorSourceManager(reactApplicationContext));
        managers.add(new RCTMGLShapeSourceManager(reactApplicationContext));
        managers.add(new RCTMGLRasterSourceManager(reactApplicationContext));
        managers.add(new RCTMGLImageSourceManager());

        // images
        managers.add(new RCTMGLImagesManager(reactApplicationContext));

        // layers
        managers.add(new RCTMGLFillLayerManager());
        managers.add(new RCTMGLFillExtrusionLayerManager());
        managers.add(new RCTMGLHeatmapLayerManager());
        managers.add(new RCTMGLLineLayerManager());
        managers.add(new RCTMGLCircleLayerManager());
        managers.add(new RCTMGLSymbolLayerManager());
        managers.add(new RCTMGLRasterLayerManager());
        managers.add(new RCTMGLBackgroundLayerManager());

        return managers;
    }
}
